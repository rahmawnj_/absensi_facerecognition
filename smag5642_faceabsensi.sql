-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 19, 2023 at 02:13 PM
-- Server version: 10.2.44-MariaDB-cll-lve
-- PHP Version: 8.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smag5642_faceabsensi`
--

-- --------------------------------------------------------

--
-- Table structure for table `attandance_devices`
--

CREATE TABLE `attandance_devices` (
  `id_attandance_device` int(11) NOT NULL,
  `device` varchar(100) NOT NULL,
  `lokasi` varchar(100) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `rfid` varchar(50) NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted` int(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `attandance_devices`
--

INSERT INTO `attandance_devices` (`id_attandance_device`, `device`, `lokasi`, `gambar`, `rfid`, `time`, `deleted`, `created_at`, `updated_at`) VALUES
(1, '1', 'tg 1', 'attandance_devices/Screenshot_(5)2.png', '5751AC7BD1', '2023-03-23 01:04:31', 0, '2023-03-23 01:04:31', '2023-02-13 04:04:29'),
(2, '', '', '', '', '2023-03-09 08:05:36', 1, '2023-02-24 00:31:05', '2023-02-17 01:54:11'),
(3, '', '90', 'attandance_devices/profile.png', '', '2023-03-09 08:05:36', 1, '2023-02-24 00:31:00', '2023-02-24 00:30:56');

-- --------------------------------------------------------

--
-- Table structure for table `attendances`
--

CREATE TABLE `attendances` (
  `id_attendance` int(11) NOT NULL,
  `id_student` int(11) NOT NULL,
  `image` text NOT NULL,
  `masuk` int(11) NOT NULL,
  `waktu_masuk` varchar(50) NOT NULL,
  `keluar` int(11) NOT NULL,
  `waktu_keluar` varchar(50) NOT NULL,
  `status_hadir` varchar(50) NOT NULL,
  `ket` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_updated` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `attendances`
--

INSERT INTO `attendances` (`id_attendance`, `id_student`, `image`, `masuk`, `waktu_masuk`, `keluar`, `waktu_keluar`, `status_hadir`, `ket`, `date`, `created_at`, `is_updated`) VALUES
(20, 23, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-04-07', '2023-04-07 03:00:02', 0),
(21, 24, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-04-07', '2023-04-07 03:00:02', 0),
(22, 23, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-04-10', '2023-04-10 03:00:02', 0),
(23, 24, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-04-10', '2023-04-10 03:00:02', 0),
(24, 23, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-04-11', '2023-04-11 03:00:02', 0),
(25, 24, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-04-11', '2023-04-11 03:00:02', 0),
(26, 23, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-04-12', '2023-04-12 03:00:02', 0),
(27, 24, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-04-12', '2023-04-12 03:00:02', 0),
(28, 23, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-04-13', '2023-04-13 03:00:02', 0),
(29, 24, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-04-13', '2023-04-13 03:00:02', 0),
(30, 23, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-04-14', '2023-04-14 03:00:02', 0),
(31, 24, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-04-14', '2023-04-14 03:00:02', 0),
(32, 23, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-04-17', '2023-04-17 03:00:02', 0),
(33, 24, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-04-17', '2023-04-17 03:00:02', 0),
(34, 23, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-04-18', '2023-04-18 03:00:01', 0),
(35, 24, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-04-18', '2023-04-18 03:00:01', 0),
(36, 23, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-04-19', '2023-04-19 03:00:01', 0),
(37, 24, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-04-19', '2023-04-19 03:00:01', 0),
(38, 23, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-04-20', '2023-04-20 03:00:01', 0),
(39, 24, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-04-20', '2023-04-20 03:00:01', 0),
(40, 23, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-04-21', '2023-04-21 03:00:01', 0),
(41, 24, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-04-21', '2023-04-21 03:00:01', 0),
(42, 23, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-04-24', '2023-04-24 03:00:01', 0),
(43, 24, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-04-24', '2023-04-24 03:00:01', 0),
(44, 23, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-04-25', '2023-04-25 03:00:01', 0),
(45, 24, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-04-25', '2023-04-25 03:00:01', 0),
(46, 23, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-04-26', '2023-04-26 03:00:02', 0),
(47, 24, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-04-26', '2023-04-26 03:00:02', 0),
(48, 23, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-04-27', '2023-04-27 03:00:02', 0),
(49, 24, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-04-27', '2023-04-27 03:00:02', 0),
(50, 23, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-04-28', '2023-04-28 03:00:01', 0),
(51, 24, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-04-28', '2023-04-28 03:00:01', 0),
(52, 23, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-01', '2023-05-01 03:00:02', 0),
(53, 24, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-01', '2023-05-01 03:00:02', 0),
(54, 23, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-02', '2023-05-02 03:00:02', 0),
(55, 24, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-02', '2023-05-02 03:00:02', 0),
(56, 23, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-03', '2023-05-03 03:00:02', 0),
(57, 24, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-03', '2023-05-03 03:00:02', 0),
(58, 23, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-04', '2023-05-04 03:00:01', 0),
(59, 24, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-04', '2023-05-04 03:00:01', 0),
(60, 23, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-05', '2023-05-05 03:00:02', 0),
(61, 24, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-05', '2023-05-05 03:00:02', 0),
(62, 23, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-08', '2023-05-08 03:00:02', 0),
(63, 24, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-08', '2023-05-08 03:00:02', 0),
(64, 23, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-09', '2023-05-09 03:00:02', 0),
(65, 24, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-09', '2023-05-09 03:00:02', 0),
(66, 23, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-10', '2023-05-10 03:00:02', 0),
(67, 24, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-10', '2023-05-10 03:00:02', 0),
(68, 31, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-10', '2023-05-10 03:00:02', 0),
(69, 32, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-10', '2023-05-10 03:00:02', 0),
(70, 33, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-10', '2023-05-10 03:00:02', 0),
(71, 34, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-10', '2023-05-10 03:00:02', 0),
(72, 35, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-10', '2023-05-10 03:00:02', 0),
(73, 36, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-10', '2023-05-10 03:00:02', 0),
(74, 37, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-10', '2023-05-10 03:00:02', 0),
(75, 38, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-10', '2023-05-10 03:00:02', 0),
(76, 39, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-10', '2023-05-10 03:00:02', 0),
(77, 40, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-10', '2023-05-10 03:00:02', 0),
(78, 41, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-10', '2023-05-10 03:00:02', 0),
(79, 42, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-10', '2023-05-10 03:00:02', 0),
(80, 43, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-10', '2023-05-10 03:00:02', 0),
(81, 44, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-10', '2023-05-10 03:00:02', 0),
(82, 45, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-10', '2023-05-10 03:00:02', 0),
(83, 46, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-10', '2023-05-10 03:00:02', 0),
(84, 47, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-10', '2023-05-10 03:00:02', 0),
(85, 48, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-10', '2023-05-10 03:00:02', 0),
(86, 49, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-10', '2023-05-10 03:00:02', 0),
(87, 50, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-10', '2023-05-10 03:00:02', 0),
(88, 51, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-10', '2023-05-10 03:00:02', 0),
(89, 52, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-10', '2023-05-10 03:00:02', 0),
(90, 53, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-10', '2023-05-10 03:00:02', 0),
(91, 54, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-10', '2023-05-10 03:00:02', 0),
(92, 55, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-10', '2023-05-10 03:00:02', 0),
(93, 56, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-10', '2023-05-10 03:00:02', 0),
(94, 57, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-10', '2023-05-10 03:00:02', 0),
(95, 58, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-10', '2023-05-10 03:00:02', 0),
(96, 59, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-10', '2023-05-10 03:00:02', 0),
(97, 60, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-10', '2023-05-10 03:00:02', 0),
(98, 61, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-10', '2023-05-10 03:00:02', 0),
(99, 62, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-10', '2023-05-10 03:00:02', 0),
(100, 63, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-10', '2023-05-10 03:00:02', 0),
(101, 64, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-10', '2023-05-10 03:00:02', 0),
(102, 65, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-10', '2023-05-10 03:00:02', 0),
(103, 66, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-10', '2023-05-10 03:00:02', 0),
(104, 23, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-11', '2023-05-11 03:00:01', 0),
(105, 24, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-11', '2023-05-11 03:00:01', 0),
(106, 31, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-11', '2023-05-11 03:00:01', 0),
(107, 32, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-11', '2023-05-11 03:00:01', 0),
(108, 33, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-11', '2023-05-11 03:00:02', 0),
(109, 34, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-11', '2023-05-11 03:00:02', 0),
(110, 35, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-11', '2023-05-11 03:00:02', 0),
(111, 36, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-11', '2023-05-11 03:00:02', 0),
(112, 37, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-11', '2023-05-11 03:00:02', 0),
(113, 38, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-11', '2023-05-11 03:00:02', 0),
(114, 39, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-11', '2023-05-11 03:00:02', 0),
(115, 40, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-11', '2023-05-11 03:00:02', 0),
(116, 41, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-11', '2023-05-11 03:00:02', 0),
(117, 42, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-11', '2023-05-11 03:00:02', 0),
(118, 43, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-11', '2023-05-11 03:00:02', 0),
(119, 44, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-11', '2023-05-11 03:00:02', 0),
(120, 45, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-11', '2023-05-11 03:00:02', 0),
(121, 46, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-11', '2023-05-11 03:00:02', 0),
(122, 47, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-11', '2023-05-11 03:00:02', 0),
(123, 48, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-11', '2023-05-11 03:00:02', 0),
(124, 49, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-11', '2023-05-11 03:00:02', 0),
(125, 50, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-11', '2023-05-11 03:00:02', 0),
(126, 51, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-11', '2023-05-11 03:00:02', 0),
(127, 52, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-11', '2023-05-11 03:00:02', 0),
(128, 53, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-11', '2023-05-11 03:00:02', 0),
(129, 54, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-11', '2023-05-11 03:00:02', 0),
(130, 55, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-11', '2023-05-11 03:00:02', 0),
(131, 56, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-11', '2023-05-11 03:00:02', 0),
(132, 57, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-11', '2023-05-11 03:00:02', 0),
(133, 58, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-11', '2023-05-11 03:00:02', 0),
(134, 59, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-11', '2023-05-11 03:00:02', 0),
(135, 60, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-11', '2023-05-11 03:00:02', 0),
(136, 61, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-11', '2023-05-11 03:00:02', 0),
(137, 62, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-11', '2023-05-11 03:00:02', 0),
(138, 63, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-11', '2023-05-11 03:00:02', 0),
(139, 64, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-11', '2023-05-11 03:00:02', 0),
(140, 65, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-11', '2023-05-11 03:00:02', 0),
(141, 66, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-11', '2023-05-11 03:00:02', 0),
(142, 23, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-12', '2023-05-12 03:00:02', 0),
(143, 24, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-12', '2023-05-12 03:00:02', 0),
(144, 31, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-12', '2023-05-12 03:00:02', 0),
(145, 32, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-12', '2023-05-12 03:00:02', 0),
(146, 33, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-12', '2023-05-12 03:00:02', 0),
(147, 34, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-12', '2023-05-12 03:00:02', 0),
(148, 35, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-12', '2023-05-12 03:00:02', 0),
(149, 36, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-12', '2023-05-12 03:00:02', 0),
(150, 37, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-12', '2023-05-12 03:00:02', 0),
(151, 38, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-12', '2023-05-12 03:00:02', 0),
(152, 39, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-12', '2023-05-12 03:00:02', 0),
(153, 40, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-12', '2023-05-12 03:00:03', 0),
(154, 41, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-12', '2023-05-12 03:00:03', 0),
(155, 42, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-12', '2023-05-12 03:00:03', 0),
(156, 43, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-12', '2023-05-12 03:00:03', 0),
(157, 44, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-12', '2023-05-12 03:00:03', 0),
(158, 45, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-12', '2023-05-12 03:00:03', 0),
(159, 46, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-12', '2023-05-12 03:00:03', 0),
(160, 47, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-12', '2023-05-12 03:00:03', 0),
(161, 48, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-12', '2023-05-12 03:00:03', 0),
(162, 49, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-12', '2023-05-12 03:00:03', 0),
(163, 50, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-12', '2023-05-12 03:00:03', 0),
(164, 51, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-12', '2023-05-12 03:00:03', 0),
(165, 52, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-12', '2023-05-12 03:00:03', 0),
(166, 53, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-12', '2023-05-12 03:00:03', 0),
(167, 54, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-12', '2023-05-12 03:00:03', 0),
(168, 55, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-12', '2023-05-12 03:00:03', 0),
(169, 56, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-12', '2023-05-12 03:00:03', 0),
(170, 57, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-12', '2023-05-12 03:00:03', 0),
(171, 58, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-12', '2023-05-12 03:00:03', 0),
(172, 59, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-12', '2023-05-12 03:00:03', 0),
(173, 60, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-12', '2023-05-12 03:00:03', 0),
(174, 61, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-12', '2023-05-12 03:00:03', 0),
(175, 62, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-12', '2023-05-12 03:00:03', 0),
(176, 63, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-12', '2023-05-12 03:00:03', 0),
(177, 64, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-12', '2023-05-12 03:00:03', 0),
(178, 65, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-12', '2023-05-12 03:00:03', 0),
(179, 66, '', 0, '-', 0, '-', 'Alfa', 'Alfa', '2023-05-12', '2023-05-12 03:00:03', 0);

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE `classes` (
  `id_class` int(11) NOT NULL,
  `kelas` varchar(50) NOT NULL,
  `deleted` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `classes`
--

INSERT INTO `classes` (`id_class`, `kelas`, `deleted`, `created_at`, `updated_at`) VALUES
(0, '57984543', 1, '2022-11-08 07:25:14', NULL),
(2, 'Grade 11', 0, '2023-03-03 03:43:30', '2023-04-03 06:17:19'),
(3, 'Grade 11 MIA', 1, '2023-03-03 03:43:42', NULL),
(4, 'Grade 11 IIS', 1, '2023-03-03 03:43:52', NULL),
(5, 'Grade 12 MIA', 1, '2023-03-03 03:44:00', NULL),
(6, 'Grade 12 IIS', 1, '2023-03-03 03:44:10', NULL),
(7, '12', 1, '2023-04-04 21:09:02', NULL),
(8, 'X1', 0, '2023-05-08 21:17:51', NULL),
(9, 'X2', 0, '2023-05-08 21:17:59', NULL),
(10, 'X3', 0, '2023-05-08 21:18:18', NULL),
(11, 'X4', 0, '2023-05-08 21:18:27', NULL),
(12, 'X5', 0, '2023-05-08 21:19:26', NULL),
(13, 'X6', 0, '2023-05-08 21:19:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `holidays`
--

CREATE TABLE `holidays` (
  `id_holiday` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `waktu` date NOT NULL,
  `type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `holidays`
--

INSERT INTO `holidays` (`id_holiday`, `name`, `waktu`, `type`) VALUES
(3, 'ISA', '2023-04-11', 'student');

-- --------------------------------------------------------

--
-- Table structure for table `operational_times`
--

CREATE TABLE `operational_times` (
  `id_operational_time` int(11) NOT NULL,
  `waktu_masuk` varchar(20) NOT NULL,
  `telat` varchar(20) NOT NULL,
  `waktu_keluar` varchar(20) NOT NULL,
  `type` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `operational_times`
--

INSERT INTO `operational_times` (`id_operational_time`, `waktu_masuk`, `telat`, `waktu_keluar`, `type`) VALUES
(1, '06:00-07:00', '07:01', '15:00-16:00', 'student');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id_role` int(11) NOT NULL,
  `role` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id_role`, `role`) VALUES
(3, 'admin_absensi'),
(5, 'operator_absensi');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id_setting` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id_setting`, `name`, `value`) VALUES
(3, 'school_name', 'SMA 1 Wanasari'),
(8, 'weekend_attendance', 'on');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id_student` int(11) NOT NULL,
  `id_class` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `nisn` varchar(50) NOT NULL,
  `saldo` varchar(255) NOT NULL,
  `rfid` varchar(50) NOT NULL,
  `jenis_kelamin` varchar(10) NOT NULL,
  `pin` varchar(100) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `deleted` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id_student`, `id_class`, `nama`, `nisn`, `saldo`, `rfid`, `jenis_kelamin`, `pin`, `foto`, `deleted`, `updated_at`, `created_at`) VALUES
(1, 1, 'tes', '', 'Fw==', '', 'Perempuan', '$2y$10$todcoEBtZ1GPSzYs27POIOP8lFvHKyRpta2O8v9Kre2MPLUt2JaGu', 'students/IMG20230212155809_-_Ade_Akmaludin.jpg', 1, NULL, '2023-02-22 08:46:53'),
(2, 2, 'Rimiku Santalum Sativa', '', 'KUBF9HS0', '', 'Perempuan', '$2y$10$BTcCtKKNHsaCwfrSFQJ5l.CPbvNbXCAoy7en8uonN9KTKkl7jxHKq', 'students/graduated_-_Copy.png', 1, '2023-03-07 02:46:23', '2023-03-03 06:38:26'),
(3, 2, 'Almandiena Az-Zahra', '', 'oA==', '', 'Perempuan', '$2y$10$NS4TPggxn3/zWMWZvseuKepaoGAj0yBVPYV8TxZyWEYEJ7piYSv3G', 'students/graduated_-_Copy1.png', 1, '2023-03-06 03:53:27', '2023-03-03 06:41:54'),
(4, 3, 'DARRYL SELMA HANIFA SETIADI', '', 'PA==', '', 'Perempuan', '$2y$10$y/kq9tOH2c2cQs/XDojOneYiXlrZi2II3FQWKF9hS8ckJENb.zSn6', 'students/graduated_-_Copy3.png', 1, '2023-03-06 03:54:02', '2023-03-03 06:58:05'),
(5, 3, 'THAREQ DEZAN AHSAN', '', 'Rg==', '', 'Laki-laki', '$2y$10$I85B9ZrsVYKTztf0uX4PLOsVf8x7gq6Oa/hs287PgeVfmdeFi5cHe', 'students/graduated_-_Copy4.png', 1, '2023-03-06 03:54:16', '2023-03-03 07:01:18'),
(6, 4, 'MUHAMMAD BAGUS BURHAN', '', 'pQ==', '', 'Laki-laki', '$2y$10$TPpsLlqREQHOqWVnl7O3LemW46tBPuoihtS0LOG/iPIGg8/3wCYVG', 'students/graduated_-_Copy5.png', 1, '2023-03-06 03:54:26', '2023-03-03 07:02:58'),
(7, 4, 'FADILLAH RAHMANIA', '', 'CQ==', '', 'Perempuan', '$2y$10$mHu4HIKpmHYOzpMAoviCVe/WSSmrBpNJirda8blY/5gUMocZ2oAv.', 'students/graduated_-_Copy6.png', 1, '2023-03-06 03:54:36', '2023-03-03 07:04:40'),
(8, 6, 'Aqiilah Nur Zalfa', '', 'Fw==', '', 'Perempuan', '$2y$10$btIQTCZIB.vfXJtmlB8evO2Q7QlVCcNsAYlY1/7Mynh.rwQP62HAO', 'students/Female_Teacher6_(1)2.jpg', 1, '2023-03-27 02:07:17', '2023-03-03 07:06:05'),
(9, 6, 'Qurrota A\'yun Nugraha', '', 'nw==', '', 'Perempuan', '$2y$10$/53GScnxDRAhlrIQenuTOu3nw4HSaTkZV6sPNDDksvVbvuwzaFr3C', 'students/graduated_-_Copy11.png', 1, '2023-03-06 03:55:19', '2023-03-03 07:07:54'),
(10, 5, 'Tasya Alifia Febrianty', '', 'ug==', '', 'Perempuan', '$2y$10$.uXGtHKEZ386GLxMQSvKn.IwWrUZWWgLekWjqog6FVrWccbGpD5hO', 'students/Female_Teacher6_(1)1.jpg', 1, '2023-03-27 02:07:06', '2023-03-03 07:13:28'),
(11, 5, 'Titah Wahyu Fitria Anantya', '', 'HQ==', '', 'Perempuan', '$2y$10$ceBoXBr.W.3LXEXJIF6U3uo9ghb4Y2clpz7xu9Sfc6GICKMCefXIC', 'students/Female_Teacher6_(1).jpg', 1, '2023-03-27 02:06:50', '2023-03-03 07:14:59'),
(12, 3, 'gggg', '', 'rw==', '', 'Laki-laki', '$2y$10$Aw1q201KGz9v35WVBDV4belnltkFoeCpGre5YEAclhty15zRMtwdK', 'students/sandy-nugroho.png', 1, '2023-03-06 03:56:05', '2023-03-06 03:55:52'),
(13, 3, '564765765', '', 'mw==', '', 'Perempuan', '$2y$10$caNJ.T4QhfgJtu/sLZpvdO4xJoxGMifxQgNCWwAaT1d5k8dyPX.cy', 'students/graduated_-_Copy12.png', 1, NULL, '2023-03-06 04:02:03'),
(14, 4, 'rahma', '', 'a5fGBLps', '', 'Perempuan', '$2y$10$63SzpusVFKKwekv.M59YZOc9UehxTylnyfwmygcr/nxVnHhR0zQJK', 'students/cdbb36e2-af59-43fb-bfde-ba71f5287d8e.jpg', 1, '2023-03-07 02:35:12', '2023-03-07 02:30:23'),
(15, 3, 'Rahma', '', 'HC5a5d62', '', 'Laki-laki', '$2y$10$DNVVnA1/iWDpmqZOJsN8QuQM9Lkd/KLZ.M.Pc.VTr72Px0v3xCRwu', 'students/Female_Teacher6.jpg', 1, '2023-03-27 02:06:23', '2023-03-07 03:00:29'),
(16, 5, 'ioioio', '', 'mA==', '', 'Laki-laki', '$2y$10$byX4mzmX/LFWtHWRkQRhBeW/QNLhkD.AhR.83jmIlJcXNVEbKMQxq', 'students/', 1, NULL, '2023-03-23 07:24:50'),
(17, 5, 'tyui', '', 'Sg==', '', 'Laki-laki', '$2y$10$qVwMAj/R59M1I6agg3d1.O7z9h10EdxfxauZ/0CNk8fHBZxIhGrwC', 'students/Screenshot_(51).png', 1, '2023-03-23 07:26:18', '2023-03-23 07:26:08'),
(18, 3, '45654', '', 'tQ==', '', 'Laki-laki', '$2y$10$RrixzyMQeNu/XBhbXFELBOPo9Cx3EKJNMYvrdXCsI6wHI4e9XwBLu', 'students/employer.png', 1, NULL, '2023-03-24 08:31:41'),
(23, 2, 'Hanif Syafiul Huda', '20041098', '', 'e314a91d43', 'Laki-laki', '', 'students/IMG_20230427_145908.jpg', 0, '2023-04-27 01:00:12', '2023-04-01 18:48:24'),
(24, 2, 'MOH. IQBAL SYAROF AL HAFIZ', '20041097', '', '90e5a020f5', 'Laki-laki', '', 'students/aku.jpg', 0, '2023-05-09 01:43:53', '2023-04-02 08:54:05'),
(25, 2, '534543', '', '', '', 'Laki-laki', '', 'students/Female_Teacher6_(1).jpg', 1, NULL, '2023-04-04 02:00:50'),
(26, 2, 'uy', '', '', '', 'Laki-laki', '', 'students/Female_Teacher6_(1).jpg', 1, '2023-04-04 02:03:53', '2023-04-04 02:03:13'),
(27, 0, 'Putra', '', '', '', 'Laki-laki', '', '', 1, NULL, NULL),
(28, 0, 'Putri', '', '', '', 'Perempuan', '', '', 1, NULL, NULL),
(29, 0, 'Putra', '', '', '', 'Laki-laki', '', '', 1, NULL, NULL),
(30, 0, 'Putri', '', '', '', 'Perempuan', '', '', 1, NULL, NULL),
(31, 0, 'ADNAN MAULANA NOVELIYANTO', '', '', '', 'Laki-laki', '', '', 1, NULL, NULL),
(32, 0, 'AFUWU ROHMAH', '0072015351', '', '234c8a43a6', 'Perempuan', '', '', 0, NULL, NULL),
(33, 0, 'AHMAD FAUZAN HADIWINOTO', '0066634341', '', 'a3c358427a', 'Laki-laki', '', '', 0, NULL, NULL),
(34, 0, 'AMELIYAH', '', '', '', 'Perempuan', '', '', 1, NULL, NULL),
(35, 0, 'ANGGER PURNOMO', '0075437119', '', 'd3164242c5', 'Laki-laki', '', '', 0, NULL, NULL),
(36, 0, 'DEN NOVITA', '0061529809', '', '7333224321', 'Perempuan', '', '', 0, NULL, NULL),
(37, 0, 'DONI ADHITYA PEDROSA', '0076950177', '', '83dfe243fd', 'Laki-laki', '', '', 0, NULL, NULL),
(38, 0, 'FATHIN HADIKA ARYA', '0088908192', '', '830cf6433a', 'Perempuan', '', '', 0, NULL, NULL),
(39, 0, 'HARUN AHMAD BAHDAWI', '0075040612', '', '73fba34368', 'Laki-laki', '', '', 0, NULL, NULL),
(40, 0, 'HILAL GHOZALI', '0071412760', '', '73da4243a8', 'Laki-laki', '', '', 0, NULL, NULL),
(41, 0, 'INTAN FEBIYANTI', '0062933708', '', '3381ec431d', 'Perempuan', '', '', 0, NULL, NULL),
(42, 0, 'KHOTMILATUL', '0074699828', '', '7342404332', 'Perempuan', '', '', 0, NULL, NULL),
(43, 0, 'LAELATUL HIKMAH', '0085508448', '', 'd3da0b4240', 'Perempuan', '', '', 0, NULL, NULL),
(44, 0, 'LAILI NUR FAIZAH', '0076885705', '', 'a3b4df428a', 'Perempuan', '', '', 0, NULL, NULL),
(45, 0, 'LUTFI SARAH', '0062685712', '', 'b3925b4238', 'Perempuan', '', '', 0, NULL, NULL),
(46, 0, 'M. MA&#39;RUF KHAMALUDIN', '0052520348', '', '336d80439d', 'Perempuan', '', '', 0, NULL, NULL),
(47, 0, 'MAHARANI AYU SAFITRI', '0079026983', '', 'f3893c4204', 'Perempuan', '', '', 0, NULL, NULL),
(48, 0, 'MAHASIN MAULIDIYA', '0076742098', '', '83bb794302', 'Perempuan', '', '', 0, NULL, NULL),
(49, 0, 'MAY TRIANINGSIH', '0084029171', '', 'b308f5420c', 'Perempuan', '', '', 0, NULL, NULL),
(50, 0, 'MOHAMMAD RAFI', '0076458288', '', '13ad1043ed', 'Laki-laki', '', '', 0, NULL, NULL),
(51, 0, 'MUHAMMAD DWI CHILDA YUNESKA', '0079306882', '', '230d10437d', 'Laki-laki', '', '', 0, NULL, NULL),
(52, 0, 'MUHAMMAD ILZAM AINUSSYIFA', '0078115775', '', '7359e7438e', 'Laki-laki', '', '', 0, NULL, NULL),
(53, 0, 'MUHAMMAD KHASBI ASSHIDDIQ', '0078125040', '', '13b0b04350', 'Laki-laki', '', '', 0, NULL, NULL),
(54, 0, 'NAIFA WIDYA PRAMESTI', '0065535187', '', '137972435b', 'Perempuan', '', '', 0, NULL, NULL),
(55, 0, 'NATASYA PUTRI HADIANSYAH', '0074720717', '', '13e56743d2', 'Perempuan', '', '', 0, NULL, NULL),
(56, 0, 'NURUL MA&#39;RIFAH', '0075837669', '', '73e9fe4327', 'Perempuan', '', '', 0, NULL, NULL),
(57, 0, 'RAGIL SAPUTRA', '0072835883', '', '8395c04395', 'Laki-laki', '', '', 0, NULL, NULL),
(58, 0, 'RAHMA YULIANTIKA', '0082477793', '', 'e3034a42e8', 'Perempuan', '', '', 0, NULL, NULL),
(59, 0, 'RICKY SETYO MANDALA', '0071719215', '', '13a69e4368', 'Laki-laki', '', '', 0, NULL, NULL),
(60, 0, 'RISQA VIANA', '0076587322', '', '13f1374396', 'Perempuan', '', '', 0, NULL, NULL),
(61, 0, 'SAFINA', '0074496056', '', '83ddbf43a2', 'Perempuan', '', '', 0, NULL, NULL),
(62, 0, 'SULIS SETIOWATI', '0079414726', '', '738ccf4373', 'Perempuan', '', '', 0, NULL, NULL),
(63, 0, 'TAUFIK BRIAN', '0133771030', '', '13afc3433c', 'Laki-laki', '', '', 0, NULL, NULL),
(64, 0, 'ULFA DWIYANTI', '0138470391', '', '7399c5436c', 'Perempuan', '', '', 0, NULL, NULL),
(65, 0, 'WIBOWO ADI PRATAMA', '0085466824', '', '83d895438d', 'Laki-laki', '', '', 0, NULL, NULL),
(66, 0, 'ZAKIYYATUL FIKRI', '0088860004', '', '8351dd434c', 'Perempuan', '', '', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `student_attandances`
--

CREATE TABLE `student_attandances` (
  `id_student_attandance` int(11) NOT NULL,
  `id_device` int(11) NOT NULL,
  `id_student` int(11) NOT NULL,
  `masuk` int(11) NOT NULL,
  `waktu_masuk` varchar(50) NOT NULL,
  `keluar` int(11) DEFAULT NULL,
  `waktu_keluar` varchar(50) DEFAULT NULL,
  `status_hadir` varchar(11) NOT NULL,
  `ket` varchar(100) NOT NULL,
  `edited_by` varchar(100) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_updated` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `id_role` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_user`, `id_role`, `nama`, `foto`, `username`, `password`, `created_at`, `updated_at`, `deleted`) VALUES
(1, 1, 'Administrator', 'users/graduated_-_Copy.png', 'admin', '$2y$10$h2uwSZch946wMiBjYjICVeZLxaHZTPllPiHB6f0ApnBsAiuSWAT1C', NULL, '2023-03-06 03:58:35', 0),
(2, 2, 'Admin Topup', 'users/graduated_-_Copy3.png', 'topup', '$2y$10$QQpsvEe8NjVEq/6c6dNZhu/j91igbtkP3Q9u60EbPR8GUiaDu3JeC', '2022-07-31 12:01:18', '2023-03-06 04:00:55', 0),
(3, 1, 'Admin', 'users/graduated_-_Copy4.png', 'administrator', '$2a$12$uYMT8ZfTP25x6b6ZuwjyoeWCUHJYYCfmKPy5zV4JZH9TrItUWWHxW', NULL, '2023-03-06 04:01:03', 1),
(4, 3, 'Admin Absensi', 'users/graduated_-_Copy5.png', 'absensi', '$2a$12$yS4RLWB.qfNiwJnowDa06.NZB6ntXy6woRXIsMJqG5sXw8lbxPh8S', NULL, '2023-03-06 04:01:12', 0),
(8, 4, 'Viewer', 'users/graduated_-_Copy2.png', 'viewer', '$2y$10$Dy2xAk0PLwFJQypHpwgFu.om1.ISDlzPmeUwKsKlW94ChGgBa5Pda', '2023-02-15 07:23:01', '2023-03-06 04:00:48', 0);

-- --------------------------------------------------------

--
-- Table structure for table `weekly_holidays`
--

CREATE TABLE `weekly_holidays` (
  `id_weekly_holiday` int(11) NOT NULL,
  `hari` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `weekly_holidays`
--

INSERT INTO `weekly_holidays` (`id_weekly_holiday`, `hari`) VALUES
(1, 'Sunday'),
(2, 'Saturday');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attandance_devices`
--
ALTER TABLE `attandance_devices`
  ADD PRIMARY KEY (`id_attandance_device`);

--
-- Indexes for table `attendances`
--
ALTER TABLE `attendances`
  ADD PRIMARY KEY (`id_attendance`);

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`id_class`);

--
-- Indexes for table `holidays`
--
ALTER TABLE `holidays`
  ADD PRIMARY KEY (`id_holiday`);

--
-- Indexes for table `operational_times`
--
ALTER TABLE `operational_times`
  ADD PRIMARY KEY (`id_operational_time`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id_role`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id_setting`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id_student`);

--
-- Indexes for table `student_attandances`
--
ALTER TABLE `student_attandances`
  ADD PRIMARY KEY (`id_student_attandance`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `weekly_holidays`
--
ALTER TABLE `weekly_holidays`
  ADD PRIMARY KEY (`id_weekly_holiday`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attandance_devices`
--
ALTER TABLE `attandance_devices`
  MODIFY `id_attandance_device` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `attendances`
--
ALTER TABLE `attendances`
  MODIFY `id_attendance` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=180;

--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
  MODIFY `id_class` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `holidays`
--
ALTER TABLE `holidays`
  MODIFY `id_holiday` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `operational_times`
--
ALTER TABLE `operational_times`
  MODIFY `id_operational_time` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id_role` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id_setting` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id_student` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `student_attandances`
--
ALTER TABLE `student_attandances`
  MODIFY `id_student_attandance` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `weekly_holidays`
--
ALTER TABLE `weekly_holidays`
  MODIFY `id_weekly_holiday` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
