<?php $this->load->view('layout/head') ?>

<div id="app" class="app app-header-fixed app-sidebar-fixed">
    <!-- END #header -->
    <?php $this->load->view('layout/header') ?>
    <!-- BEGIN #sidebar -->
    <?php $this->load->view('layout/sidebar') ?>

    <div id="content" class="app-content">
        <!-- BEGIN breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
            <li class="breadcrumb-item"><a href="javascript:;"><?= $title ?></a></li>
        </ol>
        <!-- END breadcrumb -->
        <!-- BEGIN page-header -->
        <!-- END page-header -->
        <!-- BEGIN row -->
        <div class="row">
            <!-- BEGIN col-2 -->

            <!-- END col-2 -->
            <!-- BEGIN col-10 -->
            <div class="col-xl-12">
                <!-- BEGIN panel -->
                <div class="panel panel-inverse">
                    <!-- BEGIN panel-heading -->
                    <div class="panel-heading">
                        <h4 class="panel-title"><?= $title ?></h4>
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i class="fa fa-redo"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i class="fa fa-times"></i></a>
                        </div>
                    </div>

                    <?= $this->session->flashdata('message'); ?>

                    <div class="panel-body">
                        <?php echo form_open_multipart('settings/update'); ?>

                        <fieldset>
                            <div class="container">
                                <div class="row">
                                    <div class="text-center">
                                        <h5>Siswa</h5>
                                    </div>
                                    <div class="col-5">
                                        <label for="student_masuk">Masuk</label>
                                        <div class="input-group">
                                            <input type="time" name="waktu_student_masuk1" value="<?= explode('-', $operational_time[0]['waktu_masuk'])[0] ?>" class="form-control" id="student_masuk">
                                            <input type="time" name="waktu_student_masuk2" value="<?= explode('-', $operational_time[0]['waktu_masuk'])[1] ?>" class="form-control" id="student_masuk">
                                        </div>
                                    </div>
                                    <div class="col-5">
                                        <label for="student_keluar">Keluar</label>
                                        <div class="input-group">
                                            <input type="time" name="waktu_student_keluar1" value="<?= explode('-', $operational_time[0]['waktu_keluar'])[0] ?>" class="form-control" id="student_keluar">
                                            <input type="time" name="waktu_student_keluar2" value="<?= explode('-', $operational_time[0]['waktu_keluar'])[1] ?>" class="form-control" id="student_keluar">
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <label for="student_telat">Telat</label>
                                        <input type="time" name="telat_student" value="<?= $operational_time[0]['telat'] ?>" class="form-control" id="student_telat">
                                    </div>
                                </div>


                                <div class="row mt-5">
                                    <div class="mb-3">
                                        <div class="form-check form-switch">
                                            <input class="form-check-input" <?= ($settings_weekend_attendance['value'] ==  'on') ? 'checked' : '' ?> name="weekend_attendance" type="checkbox" role="switch" id="weekend_attendance">
                                            <label class="form-check-label" for="weekend_attendance">Weekend Attendance</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex h-100 mt-3">
                                    <div class="align-self-center mx-auto">
                                        <button type="submit" class="btn btn-primary">Update Setting</button>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <?= form_close() ?>

                    </div>
                </div>
                <!-- END panel -->
            </div>
            <!-- END col-10 -->
        </div>
        <!-- END row -->
    </div>
    <!-- END #content -->

</div>

<?php $this->load->view('layout/foot') ?>