<?php $this->load->view('layout/head') ?>

<div id="app" class="app app-header-fixed app-sidebar-fixed">
    <!-- END #header -->
    <?php $this->load->view('layout/header') ?>
    <!-- BEGIN #sidebar -->
    <?php $this->load->view('layout/sidebar') ?>

    <div id="content" class="app-content">
        <!-- BEGIN breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
            <li class="breadcrumb-item"><a href="javascript:;"><?= $title ?></a></li>
        </ol>
        <!-- END breadcrumb -->
        <!-- BEGIN page-header -->
        <!-- END page-header -->
        <!-- BEGIN row -->
        <div class="row">
            <!-- BEGIN col-2 -->

            <!-- END col-2 -->
            <!-- BEGIN col-10 -->
            <div class="col-xl-12">
                <!-- BEGIN panel -->
                <div class="panel panel-inverse">
                    <!-- BEGIN panel-heading -->
                    <div class="panel-heading">
                        <h4 class="panel-title"><?= $title ?></h4>
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i class="fa fa-redo"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i class="fa fa-times"></i></a>
                        </div>
                    </div>

                    <?= $this->session->flashdata('message'); ?>

                    <div class="panel-body">
                        <?php echo form_open_multipart('students/update'); ?>
                        <input type="hidden" class="form-control" name="id_student" value="<?= $student['id_student'] ?>">
                        <div class="img-preview d-flex">
                            <img src="<?= base_url('/assets/img/uploads/' . $student['foto']) ?>" id="gmbr" class="img-fluid img-thumbnail mx-auto d-block text-center" style="height:200px;width:200px;" alt="">
                        </div>
                        <?= $this->session->flashdata('message'); ?>
                        <fieldset>
                            <div class="form-group">
                                <label for="foto">Foto</label>
                                <input type="file" id="foto" class="form-control" size="20" name="foto" id="foto" aria-describedby="foto" placeholder="Masukan foto">
                                <span class="text-danger">
                                    <?= form_error('foto') ?>
                                </span>
                            </div>

                            <div class="form-group">
                                <label for="nama">Nama</label>
                                <input autocomplete="off" type="text" class="form-control" name="nama" value="<?= $student['nama'] ?>" id="nama" aria-describedby="nama" placeholder="Masukan Nama">
                                <span class="text-danger">
                                    <?= form_error('nama') ?>
                                </span>
                            </div>

                            <div class="form-group">
                                <label for="nisn">NISN</label>
                                <input autocomplete="off" type="number" class="form-control" value="<?= $student['nisn'] ?>" name="nisn" id="nisn" aria-describedby="nisn" placeholder="Masukan NISN" required>
                                <span class="text-danger">
                                    <?= form_error('nisn') ?>
                                </span>
                            </div>

                            <div class="form-group">
                                <label for="rfid">RFID</label>
                                <input autocomplete="off" type="text" class="form-control" name="rfid" value="<?= $student['rfid'] ?>" id="rfid" aria-describedby="rfid" placeholder="Masukan RFID" required>
                                <span class="text-danger">
                                    <?= form_error('rfid') ?>
                                </span>
                            </div>


                            <div class="form-group">
                                <label for="class">Kelas</label>
                                <select name="class" class="form-control" id="class" required>
                                    <option selected disabled>-- Pilih Kelas --</option>
                                    <?php foreach ($classes as $class) : ?>
                                        <option <?= ($student['id_class'] == $class['id_class'] ? "selected" : "") ?> value="<?= $class['id_class'] ?>"><?= $class['kelas'] ?></option>
                                    <?php endforeach ?>
                                </select>
                                <span class="text-danger">
                                    <?= form_error('class') ?>
                                </span>
                            </div>

                            <div class="form-group">
                                <label for="jenis_kelamin">Jenis Kelamin</label>
                                <select name="jenis_kelamin" class="form-control" id="jenis_kelamin" required>
                                    <option selected disabled>-- Pilih Jenis Kelamin --</option>
                                    <option <?= ($student['jenis_kelamin'] == "Laki-laki" ? "selected" : "") ?> value="Laki-laki">Laki - laki</option>
                                    <option <?= ($student['jenis_kelamin'] == "Perempuan" ? "selected" : "") ?> value="Perempuan">Perempuan</option>
                                </select>
                                <span class="text-danger">
                                    <?= form_error('jenis_kelamin') ?>
                                </span>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </fieldset>
                        <?= form_close() ?>
                    </div>
                </div>
                <!-- END panel -->
            </div>
            <!-- END col-10 -->
        </div>
        <!-- END row -->
    </div>
    <!-- END #content -->

</div>

<?php $this->load->view('layout/foot') ?>