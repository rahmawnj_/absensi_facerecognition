<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class attendances extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library(["form_validation", 'session']);
        $this->load->model([ 'student_model']);
        $this->load->helper(['form', 'url']);
        if (!$this->session->userdata('status')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
            <div class="alert-message">
            Login terlebih dahulu!
            </div>
        </div>');
            redirect('auth/login');
        }
    }

    public function report()
    {
        if ($this->session->userdata('role') !== 'admin_absensi' && $this->session->userdata('role') !== 'Viewer' && $this->session->userdata('role') !== 'operator_absensi') {
            show_404();
        }
        if ($this->input->get('startDate') && $this->input->get('endDate')) {
            $attendances = $this->db->where('attendances.date >=', $this->input->get('startDate'));
            $attendances = $this->db->where('attendances.date <=', $this->input->get('endDate'));
        }
        $attendances = $this->db->order_by('attendances.date', 'desc');
        $attendances = $this->db->where(['students.deleted' => 0, 'id_attendance !=' => 0]);        
        $attendances = $this->db->join('students', 'students.id_student = attendances.id_student');
        $attendances = $this->db->join('classes', 'classes.id_class = students.id_class');
        $attendances = $this->db->select('attendances.*, students.nama as student_nama,  kelas as student_kelas');
        $attendances = $this->db->get('attendances')->result_array();

        $attendance_by_name = array();
        $dates = array();
        foreach ($attendances as $attendance) {
            $name = $attendance["student_nama"];
            $position = $attendance["student_kelas"];
            $date = $attendance["date"];
            $status = $attendance["status_hadir"];
            $ket = $attendance["ket"];
            $attendance_by_name[$name][$date]["status"] = $status;
            $attendance_by_name[$name][$date]["ket"] = $ket;
            $attendance_by_name[$name]["position"] = $position;
            if (!in_array($date, $dates)) {
                $dates[] = $date;
            }
        }


        $data = [
            'title' => 'Absensi Siswa',
            'dates' => $dates,
            'attendance_by_name' => $attendance_by_name
        ];

        $this->load->view('dashboard/attendances/report', $data);
    }
    public function index()
    {
        if ($this->session->userdata('role') !== 'admin_absensi' && $this->session->userdata('role') !== 'Viewer' && $this->session->userdata('role') !== 'operator_absensi') {
            show_404();
        }
        if ($this->input->get('startDate') && $this->input->get('endDate')) {
            $attendances = $this->db->where('attendances.date >=', $this->input->get('startDate'));
            $attendances = $this->db->where('attendances.date <=', $this->input->get('endDate'));
        }
        $attendances = $this->db->order_by('attendances.date', 'desc');
        $attendances = $this->db->where(['students.deleted' => 0, 'id_attendance !=' => 0]);        
        $attendances = $this->db->join('students', 'students.id_student = attendances.id_student');
        $attendances = $this->db->join('classes', 'classes.id_class = students.id_class');
        $attendances = $this->db->select('attendances.*, students.nama as student_nama,  kelas as student_kelas');
        $attendances = $this->db->get('attendances')->result_array();

        $data = [
            'title' => 'Absensi Siswa',
            'attendances' => $attendances
        ];

        $this->load->view('dashboard/attendances/index', $data);
    }

    public function edit($id_attendance)
    {
        if ($this->session->userdata('role') !== 'admin_absensi' && $this->session->userdata('role') !== 'Viewer' && $this->session->userdata('role') !== 'operator_absensi') {
            show_404();
        }
        $attandance = $this->db->join('students', 'students.id_student = attendances.id_student');
        $attandance = $this->db->join('classes', 'classes.id_class = students.id_class');
        $attandance = $this->db->get_where('attendances', array('id_attendance' => $id_attendance))->row_array();
        $data = [
            'title' => 'Absensi Siswa',
            'attendance' => $attandance
        ];

        $this->load->view('dashboard/attendances/edit', $data);
    }

    public function update()
    {
        if ($this->session->userdata('role') == 'viewer') {
            show_404();
        }

        if ($this->session->userdata('role') == 'operator_absensi' && $this->input->post('is_updated') == 1) {
            show_404();
        }

        $this->form_validation->set_rules('masuk', 'Masuk', 'required|trim');
        $this->form_validation->set_rules('keluar', 'Keluar', 'required|trim');
        $this->form_validation->set_rules('status_hadir', 'Status Hadir', 'required|trim');
        $this->form_validation->set_rules('ket', 'Keterangan', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->edit($this->input->post('id_attendance'));
        } else {
            $this->db->update('attendances', [
                'is_updated' => 1,
                'masuk' => $this->input->post('masuk'),
                'keluar' => $this->input->post('keluar'),
                'status_hadir' => $this->input->post('status_hadir'),
                'ket' => $this->input->post('ket'),
            ], ['id_attendance' => $this->input->post('id_attendance')]);
            $this->session->set_flashdata('success', 'Kelas Berhasil Diperbarui!');
            redirect('attendances');
        }
    }

	public function delete($attendance)
    {
        if ($this->session->userdata('role') !== 'admin_absensi') {
            show_404();
        }
		$this->db->delete('attendances', array('id_attendance' => $attendance));        
        redirect('attendances');
    }
}
