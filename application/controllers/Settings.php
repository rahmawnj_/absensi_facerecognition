<?php
defined('BASEPATH') or exit('No direct script access allowed');

class settings extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library(["form_validation", 'session']);
        $this->load->model(['setting_model', 'operational_time_model']);
        $this->load->helper(['form', 'url']);
        if (!$this->session->userdata('status')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
            <div class="alert-message">
            Login terlebih dahulu!
            </div>
        </div>');
            redirect('auth/login');
        }
    }

    public function index()
    {
        if ($this->session->userdata('role') !== 'admin_absensi' && $this->session->userdata('role') !== 'Viewer' && $this->session->userdata('role') !== 'operator_absensi') {
            show_404();
        }
        $operational_times = $this->operational_time_model->get_operational_times();

        $data = [
            'title' => 'Setting',
            'settings_weekend_attendance' => $this->setting_model->get_setting('name', 'weekend_attendance'),
            'operational_time' => $operational_times
        ];

        $this->load->view('dashboard/settings/index', $data);
    }


    public function update()
    {
        if ($this->session->userdata('role') !== 'admin_absensi') {
            show_404();
        }

        $this->operational_time_model->update(1, [
            'waktu_masuk' => implode('-', [$this->input->post('waktu_student_masuk1'), $this->input->post('waktu_student_masuk2')]),
            'waktu_keluar' => implode('-', [$this->input->post('waktu_student_keluar1'), $this->input->post('waktu_student_keluar2')]),
            'telat' => $this->input->post('telat_student'),
        ]);
        $this->setting_model->update(8, [
            'value' => $this->input->post('weekend_attendance'),
        ]);
        $this->session->set_flashdata('success', 'Setting Secret Key Berhasil Diperbarui!');
        redirect('settings');
    }
}
